﻿#pragma strict

var bigRock : GameObject;
var throwSound : AudioSource;
var throwSpeed = 2300;

function Update() {
//if left mouse click
if(Input.GetMouseButtonDown(0)){
		
		var rock : GameObject = Instantiate(bigRock, transform.position,transform.rotation) as GameObject;
		rock.rigidbody.AddRelativeForce(Vector3.forward * throwSpeed);
		throwSound.Play();
		Destroy(rock,3);	
    }
}