﻿#pragma strict

var player : Transform;   			//our character controller
var lookDistance : float = 10.5f;	//how far too look
var minDistance : float = 10.0;		//min distance between player and enemy
var emptyPoint : Transform;			//point in x,y,z cordinate
var countPoints: float = 15.0f;		//max random generated points
var index: int =0;					//index for random spot 
var enemySpeed : float = 3.0f;     //how fast the enemy moves

var raycastHits : RaycastHit[];
var generatedSpots : Transform[];

function Start () {

	//Generate random spot  
	generatedSpots = new Transform[countPoints];

	for(var spot =0; spot != countPoints; spot ++){
	
		//get random (x,y,z) within range cordinates
		var randomPosition = Vector3(Random.Range(transform.position.x,transform.position.x+100),Random.Range(transform.position.y,transform.position.y),Random.Range(transform.position.z,transform.position.z+10));
		var clone  = GameObject.Instantiate(emptyPoint,randomPosition,Quaternion.identity);
		generatedSpots[spot] = clone; 
	}
	
	transform.position = generatedSpots[0].position;
}

function Update () {
	
	//Randomly move the enemy to random spot, and make sure nothing is infront
	if(Physics.Raycast(transform.position,Vector3.forward,0.1)){
		++index; //go to the next point
	}
	
	if(transform.position ==  generatedSpots[index].position ){
		
		index++;
	}
	if(index >= generatedSpots.Length ){
		index =0;
	}
	transform.position = Vector3.MoveTowards(transform.position, generatedSpots[index].position,enemySpeed*Time.deltaTime);
	
	//Report everything the enemy sees, ie the player 	
	raycastHits = Physics.RaycastAll(transform.position,transform.forward,lookDistance );
	
	for( var hits =0; hits != raycastHits.Length; hits++){
		if(raycastHits[hits].transform.name == "Player"){
			  
			  if(Vector3.Distance(transform.position, player.position) <= minDistance ){
				 print ("Enemy Sees Player! ... " );
				 raycastHits[hits].transform.position = Vector3.MoveTowards(transform.position,player.position, enemySpeed * Time.deltaTime);
				 
				 if(player.transform.collider){
				 
				 	
				 
				 }
				 
				 //throw cube at the player 
				 
				
				}
		}
	}
}

/*****************************************************************************
*Check if enemy is hitting the ground, or is below the ground
*
****************************************************************************/




